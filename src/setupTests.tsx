// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { MemoryHistory } from "history/createMemoryHistory";

global.withRouter = (ComponentToTest, route) => {
  const history: MemoryHistory = createMemoryHistory();
  route !== undefined && history.push(route);

  return (
    <Router history={history}>
      <ComponentToTest />
    </Router>
  );
};
