import React from "react";
import { Column, useTable } from "react-table";

type Props<T> = {
  data: Array<T>;
};

function Table({ data }: Props<{}>) {
  const dataObject = data[0];
  const columns = React.useMemo<Column<typeof dataObject>[]>(
    () =>
      Object.keys(dataObject || {}).map((column) => ({
        Header: column,
        accessor: column,
      })),
    [dataObject]
  );

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({ columns, data });

  return (
    <>
      {dataObject === undefined ? (
        <div>no data to render</div>
      ) : (
        <table {...getTableProps()} className="table-auto text-justify">
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()} className="mb-4">
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps()}
                    className="text-left capitalize p-3 text-gray-600"
                  >
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()} className="text-xs text-gray-500">
            {rows.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="p-3">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </>
  );
}

export default Table;
