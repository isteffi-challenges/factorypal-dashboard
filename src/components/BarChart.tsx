import React from "react";
import { ResponsiveBar } from "@nivo/bar";

function BarChart({ data }: any) {
  return (
    <ResponsiveBar
      data={data}
      borderRadius={15}
      keys={["value"]}
      indexBy="label"
      margin={{ right: 50, left: 50, bottom: 100, top: 50 }}
      borderColor="#ffffff"
      borderWidth={2}
      axisBottom={{
        tickSize: 0,
        tickPadding: 20,
      }}
      axisLeft={{
        tickSize: 0,
        tickPadding: 20,
        legend: "hours",
        legendPosition: "end",
      }}
      theme={{
        fontSize: 10,
        fontFamily:
          "system-ui,-apple-system,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji'",
      }}
      enableGridX={false}
      enableGridY={false}
      labelTextColor="#ffffff"
      fill={[{ match: "*", id: "colorGradient1" }]}
      defs={[
        {
          id: "colorGradient1",
          type: "linearGradient",
          colors: [
            { offset: 0, color: "#A78BFA", opacity: 1 },
            { offset: 100, color: "#A78BFA", opacity: 0.3 },
          ],
        },
      ]}
    />
  );
}

export default BarChart;
