import { render } from "@testing-library/react";
import PieChart from "./PieChart";

describe("Pie Chart", () => {
  it("should match expected configuration", () => {
    const pieChart = render(
      <PieChart
        data={[
          {
            id: "make",
            label: "make",
            value: 133,
            color: "hsl(288, 70%, 50%)",
          },
          {
            id: "java",
            label: "java",
            value: 76,
            color: "hsl(91, 70%, 50%)",
          },
          {
            id: "elixir",
            label: "elixir",
            value: 406,
            color: "hsl(172, 70%, 50%)",
          },
          {
            id: "javascript",
            label: "javascript",
            value: 277,
            color: "hsl(281, 70%, 50%)",
          },
          {
            id: "go",
            label: "go",
            value: 109,
            color: "hsl(174, 70%, 50%)",
          },
        ]}
      />
    );
    expect(pieChart.baseElement).toMatchSnapshot();
  });
});
