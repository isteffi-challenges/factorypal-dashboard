import React from "react";
import { render, screen } from "@testing-library/react";
import Table from "./Table";

describe("Table", () => {
  it("should render a table", () => {
    const table = render(<Table data={[{ col1: "foo" }]} />);
    expect(table.baseElement).toMatchSnapshot();
  });
  it("should have a column for every data property", () => {
    render(<Table data={[{ col1: "foo", col2: "bar" }]} />);
    const columnHeaders = screen.getAllByRole("columnheader");
    expect(columnHeaders).toHaveLength(2);
    expect(columnHeaders[0]).toHaveTextContent("col1");
    expect(columnHeaders[1]).toHaveTextContent("col2");
  });
  it("should handle empty data", () => {
    render(<Table data={[]} />);
    expect(screen.getByText("no data to render")).toBeInTheDocument();
  });
});
