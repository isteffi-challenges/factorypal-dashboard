import { ResponsivePie } from "@nivo/pie";
import React from "react";

const PieChart = ({ data }: any) => (
  <ResponsivePie
    data={data}
    margin={{ right: 50, left: 50, bottom: 50, top: 50 }}
    innerRadius={0.3}
    padAngle={0.9}
    cornerRadius={20}
    activeOuterRadiusOffset={20}
    borderColor="#ffffff"
    borderWidth={2}
    arcLabelsTextColor="#ffffff"
    theme={{
      fontSize: 14,
      fontFamily:
        "system-ui,-apple-system,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji'",
    }}
    fill={[
      { match: { id: "unexplained" }, id: "dots" },
      { match: "*", id: "colorGradient2" },
    ]}
    defs={[
      {
        id: "dots",
        type: "patternDots",
        background: "#A78BFA",
        color: "rgba(255, 255, 255, 0.3)",
        size: 24,
        padding: 1,
        stagger: true,
      },
      {
        id: "colorGradient2",
        type: "linearGradient",
        colors: [
          { offset: 0, color: "#EC4899", opacity: 1 },
          { offset: 100, color: "#EC4899", opacity: 0.3 },
        ],
      },
    ]}
  />
);

export default PieChart;
