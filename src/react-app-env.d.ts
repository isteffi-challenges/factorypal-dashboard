/// <reference types="react-scripts" />

declare function withRouter(
  ComponentToTest: React.ComponentType<unknown>,
  route?: string
): any;
