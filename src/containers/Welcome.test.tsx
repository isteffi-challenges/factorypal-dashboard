import React from "react";
import { render, screen } from "@testing-library/react";
import Welcome from "./Welcome";

describe("Welcome", () => {
  it("should render text 'Welcome'", () => {
    render(withRouter(Welcome, ""));
    expect(screen.getByText("Welcome")).toBeInTheDocument();
  });
  it("should render link to dashboard", () => {
    render(withRouter(Welcome, ""));
    expect(screen.getByRole("link")).toHaveAttribute("href", "/dashboard");
  });
});
