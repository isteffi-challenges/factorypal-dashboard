import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";
import Welcome from "./Welcome";

function App() {
  return (
    <>
      <header
        className="flex items-center justify-between flex-wrap p-6"
        style={{ height: "7vh" }}
      >
        <div className="flex items-center flex-no-shrink mr-6">
          {/*This is the tailwind logo as placeholder*/}
          <svg
            className="h-8 w-8 mr-2"
            width="54"
            height="54"
            viewBox="0 0 54 54"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z" />
          </svg>
          <span className="font-semibold text-xl tracking-tight">
            FactoryPal Dashboard
          </span>
        </div>
      </header>
      <main
        className="w-full pt-100  bg-gray-50 text-sm text-gray-700 tracking-tight"
        style={{ height: "90vh" }}
      >
        <Switch>
          <Route exact path="/">
            <Welcome />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route>
            <Redirect to="/" />
          </Route>
        </Switch>
      </main>
      <footer
        className="lex items-center justify-between flex-wrap p-3 text-right font-semibold text-xs tracking-tight"
        style={{ height: "3vh" }}
      >
        Copyright 2021, Stephanie Grieger
      </footer>
    </>
  );
}

export default App;
