import React from "react";
import { Link, Route, Switch, useLocation } from "react-router-dom";
import logs, { Log } from "../assets/logs";
import BarChart from "../components/BarChart";
import PieChart from "../components/PieChart";
import Table from "../components/Table";

const renderDataAsText = (data: Log[]) => {
  return (
    <div className="flex justify-between">
      {data.map((d, index) => (
        <div
          className="bg-white rounded-xl shadow-2xl inline-flex flex-col items-center w-1/4 p-2"
          key={`data-as-text-${index}`}
        >
          <div className="text-3xl mx-auto my-2 bg-pink-500 w-12 h-12 rounded-xl px-3 py-2 text-white font-bold">
            {d.type === "number" ? (d.value < 0 ? "↓" : "↑") : "%"}
          </div>
          <div className="text-xs uppercase text-center my-1">
            {d.description}
          </div>
          <div className="text-5xl text-center mt-1">{d.value}</div>
        </div>
      ))}
    </div>
  );
};

const renderDataAsTable = (
  data: Log[],
  tableName: string,
  ...excludes: (keyof Log)[]
) => {
  const cleanData = (d: Log[]) =>
    d.map((log) => {
      let cleanedLog: Partial<Log> = log;
      for (let exclude of excludes) {
        const { [exclude]: _, ...clean } = cleanedLog;
        cleanedLog = clean;
      }
      return cleanedLog;
    });

  return (
    <div className="w-auto h-auto bg-white rounded-xl p-6 m-8 shadow-2xl capitalize overflow-scroll">
      <div className="text-2xl text-center mb-8 bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 p-3 rounded-xl font-bold text-white">
        {tableName}
      </div>
      <Table data={cleanData(data)} />
    </div>
  );
};

const renderDataAsChart = (
  data: Log[],
  chartName: string,
  chartType: "bar" | "pie"
) => {
  const convertDataTypes = (d: Log[]) =>
    d.map((log) => {
      let value = log.value;
      value = log.type === "secs" ? value / 60 / 60 : value;
      value = log.type === "mins" ? value / 60 : value;
      value = Math.round(value * 100) / 100;
      // ... more type conversions can be done here
      return { ...log, value };
    });

  return (
    <div className="w-2/4 h-2/4">
      <div className="text-3xl text-center capitalize">{chartName}</div>
      <div className="w-full h-full">
        {chartType === "bar" && <BarChart data={convertDataTypes(data)} />}
        {chartType === "pie" && <PieChart data={convertDataTypes(data)} />}
      </div>
    </div>
  );
};

function Dashboard() {
  let location = useLocation();

  const efficiencyData = React.useMemo(
    () => logs.filter((log) => log.category === "efficiency"),
    []
  );

  const shiftData = React.useMemo(
    () => logs.filter((log) => log.category === "shift"),
    []
  );

  const downTimeData = React.useMemo(
    () => logs.filter((log) => log.category === "downtime"),
    []
  );

  return (
    <section className="w-full h-full flex overflow-hidden">
      <aside className="w-4/12">
        <Switch>
          <Route path="/dashboard/efficiency">
            {renderDataAsTable(efficiencyData, "efficiency", "id", "category")}
          </Route>
          <Route path="/dashboard/shift">
            {renderDataAsTable(shiftData, "shift", "id", "category")}
          </Route>
          <Route path="/dashboard/downtime">
            {renderDataAsTable(downTimeData, "downtime", "category")}
          </Route>
          <Route>{renderDataAsTable(logs, "recent logs", "category")}</Route>
        </Switch>
      </aside>

      <article className="w-8/12 h-full bg-gray-200 rounded-3xl p-16 flex flex-wrap items-start justify-center">
        <ul className="w-full bg-white rounded-3xl flex h-10 justify-center self-start uppercase mb-24 text-base overflow-hidden">
          <li
            className={`w-1/3 text-center py-2 border-r-2 ${
              location.pathname === "/dashboard/" &&
              "bg-purple-400 text-white h-full"
            }`}
          >
            <Link to="/dashboard/">all</Link>
          </li>
          <li
            className={`w-1/3 text-center py-2 border-r-2 ${
              location.pathname === "/dashboard/efficiency" &&
              "bg-pink-500 text-white h-full"
            }`}
          >
            <Link to="/dashboard/efficiency">efficiency</Link>
          </li>
          <li
            className={`w-1/3 text-center py-2 border-r-2 ${
              location.pathname === "/dashboard/shift" &&
              "bg-pink-500 text-white h-full"
            }`}
          >
            <Link to="/dashboard/shift">shift</Link>
          </li>
          <li
            className={`w-1/3 text-center py-2 ${
              location.pathname === "/dashboard/downtime" &&
              "bg-red-500 text-white h-full"
            }`}
          >
            <Link to="/dashboard/downtime">downtime</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/dashboard/efficiency">
            {renderDataAsText(efficiencyData)}
          </Route>
          <Route path="/dashboard/shift">
            {renderDataAsChart(shiftData, "visualisation of shift data", "bar")}
          </Route>
          <Route path="/dashboard/downtime">
            {renderDataAsChart(
              downTimeData,
              "visualisation of downtime data",
              "pie"
            )}
          </Route>
          <Route>
            {renderDataAsChart(shiftData, "visualisation of shift data", "bar")}
            {renderDataAsChart(
              downTimeData,
              "visualisation of downtime data",
              "pie"
            )}
            {renderDataAsText(efficiencyData)}
          </Route>
        </Switch>
      </article>
    </section>
  );
}

export default Dashboard;
