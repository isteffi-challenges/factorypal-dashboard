import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

jest.mock("./Dashboard", () => () => <div>dashboard</div>);
jest.mock("./Welcome", () => () => <div>welcome</div>);

describe("App", () => {
  describe("routes", () => {
    it("should have a welcome route", () => {
      render(withRouter(App, "/"));
      expect(screen.getByText("welcome")).toBeInTheDocument();
    });

    it("should have a dashboard route", () => {
      render(withRouter(App, "/dashboard"));
      expect(screen.getByText("dashboard")).toBeInTheDocument();
    });

    it("should redirect non matching routes to welcome", () => {
      render(withRouter(App, "/other"));
      expect(screen.getByText("welcome")).toBeInTheDocument();
    });
  });
});
