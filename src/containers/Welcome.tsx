import React from "react";
import { Link } from "react-router-dom";

function Welcome() {
  return (
    <section className="">
      <h1>Welcome</h1>
      <Link to="/dashboard">Go to Dashboard</Link>
    </section>
  );
}

export default Welcome;
