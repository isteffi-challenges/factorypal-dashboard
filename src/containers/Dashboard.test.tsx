import { render, screen } from "@testing-library/react";
import React from "react";
import { Log } from "../assets/logs";
import Dashboard from "./Dashboard";

type TestData = Partial<Log>;

const efficiencyDataMock: TestData[] = [
  {
    value: 0.578347,
    description: "efficiency data 1",
    category: "efficiency",
  },
  {
    value: 1.53748,
    description: "efficiency data 2",
    category: "efficiency",
  },
];
const shiftDataMock: TestData[] = [
  {
    value: 3600,
    description: "shift data 1",
    type: "secs",
    category: "shift",
  },
  {
    value: 60,
    description: "shift data 2",
    type: "mins",
    category: "shift",
  },
];
const downtimeDataMock: TestData[] = [
  {
    id: "downtime-1",
    value: 1,
    description: "downtime data 1",
    category: "downtime",
  },
  {
    id: "downtime-2",
    value: 1,
    description: "downtime data 2",
    category: "downtime",
  },
];

const renderDataInStubs = (data: TestData[], stubName: string) => (
  <>
    {data.map((d: TestData, index) => (
      <span data-testid={`${stubName}-data`} key={`data-in-stubs-${index}`}>
        {JSON.stringify(d)}
      </span>
    ))}
  </>
);

jest.mock("../components/Table", () => (props: { data: TestData[] }) => (
  <div data-testid="table-stub">
    {renderDataInStubs(props.data, "table-stub")}
  </div>
));
jest.mock("../components/BarChart", () => (props: { data: TestData[] }) => (
  <div data-testid="bar-chart-stub">
    {renderDataInStubs(props.data, "bar-chart-stub")}
  </div>
));
jest.mock("../components/PieChart", () => (props: { data: TestData[] }) => (
  <div data-testid="pie-chart-stub">
    {renderDataInStubs(props.data, "pie-chart-stub")}
  </div>
));
jest.mock("../assets/logs", () => ({
  default: [...efficiencyDataMock, ...shiftDataMock, ...downtimeDataMock],
  __esModule: true,
}));

describe("Dashboard", () => {
  describe("tables", () => {
    it("should render efficiency data as table", () => {
      render(withRouter(Dashboard, "/dashboard/efficiency"));
      expect(screen.getByTestId("table-stub")).toBeInTheDocument();
      const data = screen.getAllByTestId(`table-stub-data`);
      expect(data).toHaveLength(efficiencyDataMock.length);
      expect(data[0]).toHaveTextContent(
        `{"value":0.578347,"description":"efficiency data 1"}`
      );
      expect(data[1]).toHaveTextContent(
        `{"value":1.53748,"description":"efficiency data 2"}`
      );
    });
    it("should render shift data as table", () => {
      render(withRouter(Dashboard, "/dashboard/shift"));
      expect(screen.getByTestId("table-stub")).toBeInTheDocument();
      const data = screen.getAllByTestId(`table-stub-data`);
      expect(data).toHaveLength(shiftDataMock.length);
      expect(data[0]).toHaveTextContent(
        `{"value":3600,"description":"shift data 1","type":"secs"}`
      );
      expect(data[1]).toHaveTextContent(
        `{"value":60,"description":"shift data 2","type":"mins"}`
      );
    });
    it("should render downtime data in table", () => {
      render(withRouter(Dashboard, "/dashboard/downtime"));
      expect(screen.getByTestId("table-stub")).toBeInTheDocument();
      const data = screen.getAllByTestId(`table-stub-data`);
      expect(data).toHaveLength(downtimeDataMock.length);
      expect(data[0]).toHaveTextContent(
        `{"id":"downtime-1","value":1,"description":"downtime data 1"}`
      );
      expect(data[1]).toHaveTextContent(
        `{"id":"downtime-2","value":1,"description":"downtime data 2"}`
      );
    });
    it("should render all data on non matching sub routes", () => {
      render(withRouter(Dashboard, "/dashboard/no-match"));
      const data = screen.getAllByTestId(`table-stub-data`);
      expect(data).toHaveLength(
        efficiencyDataMock.length +
          shiftDataMock.length +
          downtimeDataMock.length
      );
      expect(data[0]).toHaveTextContent(
        `{"value":0.578347,"description":"efficiency data 1"}`
      );
      expect(data[1]).toHaveTextContent(
        `{"value":1.53748,"description":"efficiency data 2"}`
      );
      expect(data[2]).toHaveTextContent(
        `{"value":3600,"description":"shift data 1","type":"secs"}`
      );
      expect(data[3]).toHaveTextContent(
        `{"value":60,"description":"shift data 2","type":"mins"}`
      );
      expect(data[4]).toHaveTextContent(
        `{"id":"downtime-1","value":1,"description":"downtime data 1"}`
      );
      expect(data[5]).toHaveTextContent(
        `{"id":"downtime-2","value":1,"description":"downtime data 2"}`
      );
    });
  });
  describe("charts", () => {
    it("should render efficiency data as text", () => {
      render(withRouter(Dashboard, "/dashboard/efficiency"));
      expect(
        screen.getByText(`${efficiencyDataMock[0].value}`)
      ).toBeInTheDocument();
      expect(
        screen.getByText(`${efficiencyDataMock[0].description}`)
      ).toBeInTheDocument();
      expect(
        screen.getByText(`${efficiencyDataMock[1].value}`)
      ).toBeInTheDocument();
      expect(
        screen.getByText(`${efficiencyDataMock[1].description}`)
      ).toBeInTheDocument();
    });
    it("should render shift data as bar chart", () => {
      render(withRouter(Dashboard, "/dashboard/shift"));
      const barChartStubDataRow = screen.getAllByTestId(`bar-chart-stub-data`);
      expect(barChartStubDataRow).toHaveLength(shiftDataMock.length);
      expect(barChartStubDataRow[0]).toHaveTextContent(
        `{"value":1,"description":"shift data 1","type":"secs","category":"shift"}`
      );
      expect(barChartStubDataRow[1]).toHaveTextContent(
        `{"value":1,"description":"shift data 2","type":"mins","category":"shift"}`
      );
    });
    it("should render downtime data as pie chart", () => {
      render(withRouter(Dashboard, "/dashboard/downtime"));
      const barChartStubDataRow = screen.getAllByTestId(`pie-chart-stub-data`);
      expect(barChartStubDataRow).toHaveLength(shiftDataMock.length);
      expect(barChartStubDataRow[0]).toHaveTextContent(
        `{"id":"downtime-1","value":1,"description":"downtime data 1","category":"downtime"}`
      );
      expect(barChartStubDataRow[1]).toHaveTextContent(
        `{"id":"downtime-2","value":1,"description":"downtime data 2","category":"downtime"}`
      );
    });
  });
});
